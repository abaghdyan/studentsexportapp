﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace StudentsExportApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int count;
            while (true)
            {
                Console.WriteLine("Please enter number of students.");
                int.TryParse(Console.ReadLine(),out count);
                if (count>0)
                    break;
                else
                {
                    Console.Clear();
                    Console.WriteLine("Error!!!");
                }
            }

            var students = CreateRandomStudents(count);
            Shuffle(students);

            Print(students);
            
            ExportStudentsToXML(students);

            var xmlstudents = ImportStudentsFromXML();
            Console.WriteLine("Student List from XML");
            Print(xmlstudents);

            Console.ReadLine();
        }

        private static void Print(List<Student> students)
        {
            if (students == null)
            {
                Console.WriteLine("Error! It's not a students list!");
                return;
            }
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine($">>>>>>>>>>>>>>>>Students List<<<<<<<<<<<<<<<<");
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write("FirstName\tLastName\tAge\tEmail\n");
            Console.ResetColor();
            foreach (var item in students)
            {
                Console.Write($"{item.FirstName}\t{item.LastName}\t{item.Age}\t{item.Email}\n");
            }
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("---------------------------------------------");
            Console.ResetColor();
            Console.WriteLine();
        }

        public static void ExportStudentsToXML(List<Student> students)
        {
            if (students == null)
            {
                return;
            }
            XDocument xdoc = new XDocument();
            XElement xmlStudents = new XElement("students");
            foreach (var student in students)
            {
                XElement xmlStudent = new XElement("student");
                xmlStudent.Add(new XElement(nameof(student.FirstName), student.FirstName));
                xmlStudent.Add(new XElement(nameof(student.LastName), student.LastName));
                xmlStudent.Add(new XElement(nameof(student.Age), student.Age));
                xmlStudent.Add(new XElement(nameof(student.Email), student.Email));
                xmlStudents.Add(xmlStudent);
            }
            xdoc.Add(xmlStudents);
            xdoc.Save("students.xml");
        }

        public static List<Student> ImportStudentsFromXML()
        {
            List<Student> students = new List<Student>();
            XmlDocument xdoc = new XmlDocument();
            try
            {
                xdoc.Load("students.xml");
                XmlElement xRoot = xdoc.DocumentElement;
                foreach (XmlElement xnode in xRoot)
                {
                    Student student = new Student();
                    foreach (XmlNode childnode in xnode.ChildNodes)
                    {
                        if (childnode.Name == nameof(Student.FirstName))
                            student.FirstName = childnode.InnerText;

                        if (childnode.Name == nameof(Student.LastName))
                            student.LastName = childnode.InnerText;

                        if (childnode.Name == nameof(Student.Age))
                            student.Age = Convert.ToInt32(childnode.InnerText);

                        if (childnode.Name == nameof(Student.Email))
                            student.Email = childnode.InnerText;
                    }
                    students.Add(student);
                }
                return students;
            }
            catch(FileNotFoundException ex)
            {
                return null;
            }
        }

        public static List<Student> CreateRandomStudents(int count)
        {
            var students = new List<Student>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                students.Add(new Student
                {
                    FirstName = $"A{i}",
                    LastName = $"A{i}yan",
                    Age = rnd.Next(16, 45),
                    Email = $"A{i}@gmail.com"
                });
            }

            return students;
        }

        public static void Shuffle<T>(List<T> source)
        {
            Random rnd = new Random();
            int n = source.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                var strr = source[k];
                source[k] = source[n];
                source[n] = strr;
            }
        }
    }
}
